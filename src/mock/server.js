import { createServer } from "miragejs"
import { get } from "lodash"
import User from "./User"

export const createMirageServer = () => createServer({
    routes() {
      this.namespace = "api"
  
      this.post("/auth/login", (schema, {requestBody}) => {
          let attrs = JSON.parse(requestBody)
          const user = get(attrs, "user", "")
          const password = get(attrs, "password", "")
          
          return new User(user, password).validate()
      })
    },
  })